function varargout = OpticalCharacterReader_JamJahanzeb_11620(varargin)
% OPTICALCHARACTERREADER_JAMJAHANZEB_11620 MATLAB code for OpticalCharacterReader_JamJahanzeb_11620.fig
%      OPTICALCHARACTERREADER_JAMJAHANZEB_11620, by itself, creates a new OPTICALCHARACTERREADER_JAMJAHANZEB_11620 or raises the existing
%      singleton*.
%
%      H = OPTICALCHARACTERREADER_JAMJAHANZEB_11620 returns the handle to a new OPTICALCHARACTERREADER_JAMJAHANZEB_11620 or the handle to
%      the existing singleton*.
%
%      OPTICALCHARACTERREADER_JAMJAHANZEB_11620('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPTICALCHARACTERREADER_JAMJAHANZEB_11620.M with the given input arguments.
%
%      OPTICALCHARACTERREADER_JAMJAHANZEB_11620('Property','Value',...) creates a new OPTICALCHARACTERREADER_JAMJAHANZEB_11620 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OpticalCharacterReader_JamJahanzeb_11620_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OpticalCharacterReader_JamJahanzeb_11620_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OpticalCharacterReader_JamJahanzeb_11620

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @OpticalCharacterReader_JamJahanzeb_11620_OpeningFcn, ...
    'gui_OutputFcn',  @OpticalCharacterReader_JamJahanzeb_11620_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OpticalCharacterReader_JamJahanzeb_11620 is made visible.
function OpticalCharacterReader_JamJahanzeb_11620_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OpticalCharacterReader_JamJahanzeb_11620 (see VARARGIN)

% Choose default command line output for OpticalCharacterReader_JamJahanzeb_11620
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OpticalCharacterReader_JamJahanzeb_11620 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OpticalCharacterReader_JamJahanzeb_11620_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in cmdselect.
function cmdselect_Callback(hObject, eventdata, handles)
% hObject    handle to cmdselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%try-catch used if an error arises while reading the image file
try
    %below code allows to select files
    imageEntered = imgetfile();
    
    %shows the image in the axes in the figure
    imshow(imageEntered, 'Parent', handles.axsImageShow)
    
    %stores the address of the file in a global variable to be used later
    handles.data=imageEntered;
    guidata(hObject, handles);
catch
    warndlg('It seems the image could not be read. Please reselect the image.','Error');
end


% --- Executes on button press in cmdrecognize.
function cmdrecognize_Callback(hObject, eventdata, handles)

try
    
    %the image is address is stored in a variable
    imageEntered = handles.data;
    
    %the image is read and stored in a variable to be used for computation
    txt = (im2bw(imread(imageEntered)));
    
    barToWait = waitbar(0,'Please wait');
    
    %changes directory to the folder which contains the data to build database
    cd('./Characters');
    
    %the uppercase letters are read
    A = regionprops(im2bw(imread('A.bmp')), 'Image');
    %resizing the images to deal with font size issue that may arise later
    %on
    for loop = 1:length(A)
        A(loop).Image = imresize(A(loop).Image,[100 100]);
    end
    
    B = regionprops(im2bw(imread('B.bmp')), 'Image');
    for loop = 1:length(B)
        B(loop).Image = imresize(B(loop).Image,[100 100]);
    end
    
    C = regionprops(im2bw(imread('C.bmp')), 'Image');
    for loop = 1:length(C)
        C(loop).Image = imresize(C(loop).Image,[100 100]);
    end
    
    D = regionprops(im2bw(imread('D.bmp')), 'Image');
    for loop = 1:length(D)
        D(loop).Image = imresize(D(loop).Image,[100 100]);
    end
    
    E = regionprops(im2bw(imread('E.bmp')), 'Image');
    for loop = 1:length(E)
        E(loop).Image = imresize(E(loop).Image,[100 100]);
    end
    
    F = regionprops(im2bw(imread('F.bmp')), 'Image');
    for loop = 1:length(F)
        F(loop).Image = imresize(F(loop).Image,[100 100]);
    end
    
    G = regionprops(im2bw(imread('G.bmp')), 'Image');
    for loop = 1:length(G)
        G(loop).Image = imresize(G(loop).Image,[100 100]);
    end
    
    H = regionprops(im2bw(imread('H.bmp')), 'Image');
    for loop = 1:length(H)
        H(loop).Image = imresize(H(loop).Image,[100 100]);
    end
    
    I = regionprops(im2bw(imread('I.bmp')), 'Image');
    for loop = 1:length(I)
        I(loop).Image = imresize(I(loop).Image,[100 100]);
    end
    
    J = regionprops(im2bw(imread('J.bmp')), 'Image');
    for loop = 1:length(J)
        J(loop).Image = imresize(J(loop).Image,[100 100]);
    end
    
    K = regionprops(im2bw(imread('K.bmp')), 'Image');
    for loop = 1:length(K)
        K(loop).Image = imresize(K(loop).Image,[100 100]);
    end
    
    L = regionprops(im2bw(imread('L.bmp')), 'Image');
    for loop = 1:length(L)
        L(loop).Image = imresize(L(loop).Image,[100 100]);
    end
    
    M = getCharacterLower(im2bw(imread('M.bmp')),35);
    for loop = 1:length(M)
        M(loop).Image = imresize(M(loop).Image,[100 100]);
    end
    
    N = regionprops(im2bw(imread('N.bmp')), 'Image');
    for loop = 1:length(N)
        N(loop).Image = imresize(N(loop).Image,[100 100]);
    end
    
    O = regionprops(im2bw(imread('O.bmp')), 'Image');
    for loop = 1:length(O)
        O(loop).Image = imresize(O(loop).Image,[100 100]);
    end
    
    P = regionprops(im2bw(imread('P.bmp')), 'Image');
    for loop = 1:length(P)
        P(loop).Image = imresize(P(loop).Image,[100 100]);
    end
    
    Q = regionprops(im2bw(imread('Q.bmp')), 'Image');
    for loop = 1:length(Q)
        Q(loop).Image = imresize(Q(loop).Image,[100 100]);
    end
    
    R = regionprops(im2bw(imread('R.bmp')), 'Image');
    for loop = 1:length(R)
        R(loop).Image = imresize(R(loop).Image,[100 100]);
    end
    
    S = regionprops(im2bw(imread('S.bmp')), 'Image');
    for loop = 1:length(S)
        S(loop).Image = imresize(S(loop).Image,[100 100]);
    end
    
    T = regionprops(im2bw(imread('T.bmp')), 'Image');
    for loop = 1:length(T)
        T(loop).Image = imresize(T(loop).Image,[100 100]);
    end
    
    U = regionprops(im2bw(imread('U.bmp')), 'Image');
    for loop = 1:length(U)
        U(loop).Image = imresize(U(loop).Image,[100 100]);
    end
    
    V = regionprops(im2bw(imread('V.bmp')), 'Image');
    for loop = 1:length(V)
        V(loop).Image = imresize(V(loop).Image,[100 100]);
    end
    
    W = getCharacterLower(im2bw(imread('W.bmp')),35);
    for loop = 1:length(W)
        W(loop).Image = imresize(W(loop).Image,[100 100]);
    end
    
    X = regionprops(im2bw(imread('X.bmp')), 'Image');
    for loop = 1:length(X)
        X(loop).Image = imresize(X(loop).Image,[100 100]);
    end
    
    Y = regionprops(im2bw(imread('Y.bmp')), 'Image');
    for loop = 1:length(Y)
        Y(loop).Image = imresize(Y(loop).Image,[100 100]);
    end
    
    Z = regionprops(im2bw(imread('Z.bmp')), 'Image');
    for loop = 1:length(Z)
        Z(loop).Image = imresize(Z(loop).Image,[100 100]);
    end
    
    %the lowercase letters are read
    a = regionprops(im2bw(imread('aLower.bmp')),'Image');
    for loop = 1:length(a)
        a(loop).Image = imresize(a(loop).Image,[100 100]);
    end
    
    b = regionprops(im2bw(imread('bLower.bmp')),'Image');
    for loop = 1:length(b)
        b(loop).Image = imresize(b(loop).Image,[100 100]);
    end
    
    c = regionprops(im2bw(imread('cLower.bmp')),'Image');
    for loop = 1:length(c)
        c(loop).Image = imresize(c(loop).Image,[100 100]);
    end
    
    d = getCharacterLower(im2bw(imread('dLower.bmp')),15);
    for loop = 1:length(d)
        d(loop).Image = imresize(d(loop).Image,[100 100]);
    end
    
    e = regionprops(im2bw(imread('eLower.bmp')),'Image');
    for loop = 1:length(e)
        e(loop).Image = imresize(e(loop).Image,[100 100]);
    end
    
    f = regionprops(im2bw(imread('fLower.bmp')),'Image');
    for loop = 1:length(f)
        f(loop).Image = imresize(f(loop).Image,[100 100]);
    end
    
    g = getCharacterLower(im2bw(imread('gLower.bmp')),15);
    for loop = 1:length(g)
        g(loop).Image = imresize(g(loop).Image,[100 100]);
    end
    
    h = regionprops(im2bw(imread('hLower.bmp')),'Image');
    for loop = 1:length(h)
        h(loop).Image = imresize(h(loop).Image,[100 100]);
    end
    
    %the function for getLowerCharacter used to deal with tittle in the
    %characters of 'i' and 'j'
    i = getCharacterLower(im2bw(imread('iLower.bmp')),10);
    for loop = 1:length(i)
        i(loop).Image = imresize(i(loop).Image,[100 100]);
    end
    
    j = getCharacterLower(im2bw(imread('jLower.bmp')),25);
    for loop = 1:length(j)
        j(loop).Image = imresize(j(loop).Image,[100 100]);
    end
    
    k = regionprops(im2bw(imread('kLower.bmp')),'Image');
    for loop = 1:length(k)
        k(loop).Image = imresize(k(loop).Image,[100 100]);
    end
    
    l = regionprops(im2bw(imread('lLower.bmp')),'Image');
    for loop = 1:length(l)
        l(loop).Image = imresize(l(loop).Image,[100 100]);
    end
    
    m = regionprops(im2bw(imread('mLower.bmp')),'Image');
    for loop = 1:length(m)
        m(loop).Image = imresize(m(loop).Image,[100 100]);
    end
    
    n = regionprops(im2bw(imread('nLower.bmp')),'Image');
    for loop = 1:length(n)
        n(loop).Image = imresize(n(loop).Image,[100 100]);
    end
    
    o = getCharacterLower(im2bw(imread('oLower.bmp')),18);
    for loop = 1:length(o)
        o(loop).Image = imresize(o(loop).Image,[100 100]);
    end
    
    p = regionprops(im2bw(imread('pLower.bmp')),'Image');
    for loop = 1:length(p)
        p(loop).Image = imresize(p(loop).Image,[100 100]);
    end
    
    q = regionprops(im2bw(imread('qLower.bmp')),'Image');
    for loop = 1:length(q)
        q(loop).Image = imresize(q(loop).Image,[100 100]);
    end
    
    r = regionprops(im2bw(imread('rLower.bmp')),'Image');
    for loop = 1:length(r)
        r(loop).Image = imresize(r(loop).Image,[100 100]);
    end
    
    s = regionprops(im2bw(imread('sLower.bmp')),'Image');
    for loop = 1:length(s)
        s(loop).Image = imresize(s(loop).Image,[100 100]);
    end
    
    t = getCharacterLower(im2bw(imread('tLower.bmp')),18);
    for loop = 1:length(t)
        t(loop).Image = imresize(t(loop).Image,[100 100]);
    end
    
    u = regionprops(im2bw(imread('uLower.bmp')),'Image');
    for loop = 1:length(u)
        u(loop).Image = imresize(u(loop).Image,[100 100]);
    end
    
    v = regionprops(im2bw(imread('vLower.bmp')),'Image');
    for loop = 1:length(v)
        v(loop).Image = imresize(v(loop).Image,[100 100]);
    end
    
    w = regionprops(im2bw(imread('wLower.bmp')),'Image');
    for loop = 1:length(w)
        w(loop).Image = imresize(w(loop).Image,[100 100]);
    end
    
    x = getCharacterLower(im2bw(imread('xLower.bmp')),18);
    for loop = 1:length(x)
        x(loop).Image = imresize(x(loop).Image,[100 100]);
    end
    
    y = getCharacterLower(im2bw(imread('yLower.bmp')),18);
    for loop = 1:length(y)
        y(loop).Image = imresize(y(loop).Image,[100 100]);
    end
    
    z = getCharacterLower(im2bw(imread('zLower.bmp')),18);
    for loop = 1:length(z)
        z(loop).Image = imresize(z(loop).Image,[100 100]);
    end
    
    
    %the special characters are read
    scforwardslash = regionprops(im2bw(imread('forwardslash.png')),'Image');
    for loop = 1:length(scforwardslash)
        scforwardslash(loop).Image = imresize(scforwardslash(loop).Image,[100 100]);
    end
    
    schyfen = regionprops(im2bw(imread('-.png')),'Image');
    for loop = 1:length(schyfen)
        schyfen(loop).Image = imresize(schyfen(loop).Image,[100 100]);
    end
    
    scplus = regionprops(im2bw(imread('+.png')),'Image');
    for loop = 1:length(scplus)
        scplus(loop).Image = imresize(scplus(loop).Image,[100 100]);
    end
    
    scasterik = regionprops(im2bw(imread('asterik.png')),'Image');
    for loop = 1:length(scasterik)
        scasterik(loop).Image = imresize(scasterik(loop).Image,[100 100]);
    end
    
    scquestion = getCharacterLower(im2bw(imread('questionmark.png')),18);
    for loop = 1:length(scquestion)
        scquestion(loop).Image = imresize(scquestion(loop).Image,[100 100]);
    end
    
    scexclamation = getCharacterLower(im2bw(imread('exclamationmark.png')),18);
    for loop = 1:length(scexclamation)
        scexclamation(loop).Image = imresize(scexclamation(loop).Image,[100 100]);
    end
    
    sccurlyclose = regionprops(im2bw(imread('}.png')),'Image');
    for loop = 1:length(sccurlyclose)
        sccurlyclose(loop).Image = imresize(sccurlyclose(loop).Image,[100 100]);
    end
    
    sccurlyopen = regionprops(im2bw(imread('{.png')),'Image');
    for loop = 1:length(sccurlyopen)
        sccurlyopen(loop).Image = imresize(sccurlyopen(loop).Image,[100 100]);
    end
    
    scroundopen = regionprops(im2bw(imread('(.png')),'Image');
    for loop = 1:length(scroundopen)
        scroundopen(loop).Image = imresize(scroundopen(loop).Image,[100 100]);
    end
    
    scroundclose = regionprops(im2bw(imread(').png')),'Image');
    for loop = 1:length(scroundclose)
        scroundclose(loop).Image = imresize(scroundclose(loop).Image,[100 100]);
    end
    
    scOne = regionprops(im2bw(imread('1.png')),'Image');
    for loop = 1:length(scOne)
        scOne(loop).Image = imresize(scOne(loop).Image,[100 100]);
    end
    
    scTwo = regionprops(im2bw(imread('2.png')),'Image');
    for loop = 1:length(scTwo)
        scTwo(loop).Image = imresize(scTwo(loop).Image,[100 100]);
    end
    
    scThree = regionprops(im2bw(imread('3.png')),'Image');
    for loop = 1:length(scThree)
        scThree(loop).Image = imresize(scThree(loop).Image,[100 100]);
    end
    
    scFour = regionprops(im2bw(imread('4.png')),'Image');
    for loop = 1:length(scFour)
        scFour(loop).Image = imresize(scFour(loop).Image,[100 100]);
    end
    
    scFive = regionprops(im2bw(imread('5.png')),'Image');
    for loop = 1:length(scFive)
        scFive(loop).Image = imresize(scFive(loop).Image,[100 100]);
    end
    
    scSix = regionprops(im2bw(imread('6.png')),'Image');
    for loop = 1:length(scSix)
        scSix(loop).Image = imresize(scSix(loop).Image,[100 100]);
    end
    
    scSeven = regionprops(im2bw(imread('7.png')),'Image');
    for loop = 1:length(scSeven)
        scSeven(loop).Image = imresize(scSeven(loop).Image,[100 100]);
    end
    
    scEight = regionprops(im2bw(imread('8.png')),'Image');
    for loop = 1:length(scEight)
        scEight(loop).Image = imresize(scEight(loop).Image,[100 100]);
    end
    
    scNine = regionprops(im2bw(imread('9.png')),'Image');
    for loop = 1:length(scNine)
        scNine(loop).Image = imresize(scNine(loop).Image,[100 100]);
    end
    
    scZero = regionprops(im2bw(imread('0.png')),'Image');
    for loop = 1:length(scZero)
        scZero(loop).Image = imresize(scZero(loop).Image,[100 100]);
    end
    
    scamp = regionprops(im2bw(imread('&.png')),'Image');
    for loop = 1:length(scamp)
        scamp(loop).Image = imresize(scamp(loop).Image,[100 100]);
    end
    
    schash = regionprops(im2bw(imread('#.png')),'Image');
    for loop = 1:length(schash)
        schash(loop).Image = imresize(schash(loop).Image,[100 100]);
    end
    
    sccomma = regionprops(im2bw(imread(',.png')),'Image');
    for loop = 1:length(sccomma)
        sccomma(loop).Image = imresize(sccomma(loop).Image,[100 100]);
    end
    
    scperiod = regionprops(im2bw(imread('..png')),'Image');
    for loop = 1:length(scperiod)
        scperiod(loop).Image = imresize(scperiod(loop).Image,[100 100]);
    end
    
    scquotation = getCharacterLower(im2bw(imread('quotationmark.png')),18);
    for loop = 1:length(scquotation)
        scquotation(loop).Image = imresize(scquotation(loop).Image,[100 100]);
    end
    
    scsemicolon = getCharacterLower(im2bw(imread('semicolon.png')),18);
    for loop = 1:length(scsemicolon)
        scsemicolon(loop).Image = imresize(scsemicolon(loop).Image,[100 100]);
    end
    
    sccolon = getCharacterLower(im2bw(imread('colon.png')),18);
    for loop = 1:length(sccolon)
        sccolon(loop).Image = imresize(sccolon(loop).Image,[100 100]);
    end
    
    
    %directory changed to previous to be able to use the functions
    cd('..')
    
    %myNewOCR function used to recognize the characters in the image
    TakeItUp = myNewOCR(txt,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,scforwardslash,schyfen,scplus,scasterik,scquestion,scexclamation,sccurlyclose,sccurlyopen,scroundopen,scroundclose,scOne,scTwo,scThree,scFour,scFive,scSix,scSeven,scEight,scNine,scZero,scamp,schash,sccomma,scperiod,scquotation ,scsemicolon, sccolon,barToWait);
    
catch
    warndlg('Please choose an image to transform.','Caution!');
end


% --- Executes on button press in cmdcompare.
function cmdcompare_Callback(hObject, eventdata, handles)
try
    %the following line enables to read file with only '*.txt' extensions
    fileName = uigetfile('*.txt');
catch
    warndlg('It seems the file could not be read. Please reselect the file.','Error');
end
%comparison of the generated file and the selected file done
visdiff('ReadImageFile.txt',fileName)


% --- Executes on button press in cmdopenfile.
function cmdopenfile_Callback(hObject, eventdata, handles)
%enables to open the generated file
edit ReadImageFile.txt;


% --- Executes on button press in cmdClose.
function cmdClose_Callback(hObject, eventdata, handles)
%deletes the instance of the GUI to close the application
delete(OpticalCharacterReader_JamJahanzeb_11620);