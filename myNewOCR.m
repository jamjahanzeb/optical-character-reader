function newcharacterArray = myNewOCR(characterArray,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,scforwardslash,schyfen,scplus,scasterik,scquestion,scexclamation,sccurlyclose,sccurlyopen,scroundopen,scroundclose,scOne,scTwo,scThree,scFour,scFive,scSix,scSeven,scEight,scNine,scZero,scamp,schash,sccomma,scperiod,scquotation ,scsemicolon, sccolon, barToWait)

%the command below are used to compute the time elapsed while working on
%the recognition
startTime = datestr(now, 'HH:MM:SS');
YearMonthDay = clock;
startTime = [num2str(YearMonthDay(1:3)),' ',startTime];
startTime = datevec(startTime,'yyyy mm dd HH:MM:SS');

%get the centroid, image and bounding box of the images in characterArray
newcharacterArray = regionprops(characterArray,{'Image','Centroid', 'BoundingBox'});

%the below is the check to see if the two consecutive images belong
%to same character or not
index = 1;

%we have the starting pixel value and the ending pixel value of each line
[startHere, endHere] = readlines(newcharacterArray);

%the total number of lines computed
numberOfLines = length(startHere);

%if there is only one line then the following commands to be executed
if(numberOfLines == 1)
    %the loop to compare with only the next image is sequence
    for count = 1:(length(newcharacterArray)-1)
        val = newcharacterArray(count).Centroid;
        val2 = newcharacterArray(count+1).Centroid;
        
        %check if the two images have centroid very close to each other
        if((abs(val(1) - val2(1)) < 6))
            val1 = newcharacterArray(count).BoundingBox;
            val2 = newcharacterArray(count+1).BoundingBox;
            
            %get the minimum of the lefttop corner
            [valX] = min(val1(1), val2(1));
            [valY] = min(val1(2), val2(2));
            valX = floor(valX)+1;
            valY = floor(valY)+1;
            
            %get the maximum rightbottom corner
            [valXW] = max(val1(1)+val1(3), val2(1)+val2(3));
            [valYW] = max(val1(2)+val1(4), val2(2)+val2(4));
            
            %the previous image is replaced with the new image
            newcharacterArray(count).Image = characterArray(valY:valYW,valX:valXW,:);
            
            %the new centroid values
            centroidvalue1 = valYW - valY;
            centroidvalue1 = valY + (centroidvalue1/2);
            newcharacterArray(count).Centroid(1) = centroidvalue1;
            
            centroidvalue2 = valXW - valX;
            centroidvalue2 = valX + (centroidvalue2/2);
            newcharacterArray(count).Centroid(2) = centroidvalue2;
            
            %the index of the repeated image is stored to be deleted later
            dis(index) = count+1;
            
            %the flag is raised to make sure that there are images to
            %delete
            index = index + 1;
        end
    end
else
    %loop to the final image
    for count = 2:(length(newcharacterArray)-1)
        val = newcharacterArray(count).Centroid;
        
        %the loop to compare with all the previously stored images
        for countSecod = 1:(count-1)
            val2 = newcharacterArray(countSecod).Centroid;
            
            %check if the two images have centroid very close to each other
            if((abs(val(1) - val2(1)) < 8) && (abs(val(2) - val2(2)) < 32))
                val1 = newcharacterArray(count).BoundingBox;
                val2 = newcharacterArray(countSecod).BoundingBox;
                
                %get the minimum of the lefttop corner
                [valX] = min(val1(1), val2(1));
                [valY] = min(val1(2), val2(2));
                valX = floor(valX)+1;
                valY = floor(valY)+1;
                
                %get the maximum rightbottom corner
                [valXW] = max(val1(1)+val1(3), val2(1)+val2(3));
                [valYW] = max(val1(2)+val1(4), val2(2)+val2(4));
                
                %the previous image is replaced with the new image
                newcharacterArray(count).Image = characterArray(valY:valYW,valX:valXW,:);
                
                %the new centroid values
                centroidvalue1 = valYW - valY;
                centroidvalue1 = valY + (centroidvalue1/2);
                newcharacterArray(count).Centroid(1) = centroidvalue1;
                
                centroidvalue2 = valXW - valX;
                centroidvalue2 = valX + (centroidvalue2/2);
                newcharacterArray(count).Centroid(2) = centroidvalue2;
                
                %the index of the repeated image is stored to be deleted later
                dis(index) = countSecod;
                
                %the flag is raised to make sure that there are images to
                %delete
                index = index+1;
            end
        end
    end
end

if(index > 1)
    
    %delete the images in reverse order, so that the incorrect image index is
    %not deleted by accident
    for count = 1:length(dis)
        
        %delete the array containing the indexes, to be reused later
        newcharacterArray(dis(length(dis)-count+1)) = [];
    end
end

%the following variables are used to compare the centroid values of images
%in different lines
centerPreviois = zeros(numberOfLines,4);
centerCurrent = zeros(numberOfLines,4);

%a string made of cells because it may have dynamic lengths
string = cell(numberOfLines, 1);

%variable to define the size of the loop
loopSize = length(newcharacterArray);

%make an array of characters to use for storing in file
characterArraywithoutSpace = cell(79,1);
characterArraywithSpace = cell(79,1);

%for without space, the following loop converts the ASCII value to the
%character and stores in the variable
for alphabetcaseLoop = 1:26
    characterArraywithoutSpace(alphabetcaseLoop) = strcat(characterArraywithoutSpace(alphabetcaseLoop), char(alphabetcaseLoop+64));
    characterArraywithoutSpace(alphabetcaseLoop+26) = strcat(characterArraywithoutSpace(alphabetcaseLoop+26), char(alphabetcaseLoop+96));
end

%the following special characters are required to be stored explicitly
characterArraywithoutSpace(53) = strcat(characterArraywithoutSpace(53), '/');
characterArraywithoutSpace(54) = strcat(characterArraywithoutSpace(54), '-');
characterArraywithoutSpace(55) = strcat(characterArraywithoutSpace(55), '+');
characterArraywithoutSpace(56) = strcat(characterArraywithoutSpace(56), '*');
characterArraywithoutSpace(57) = strcat(characterArraywithoutSpace(57), '?');
characterArraywithoutSpace(58) = strcat(characterArraywithoutSpace(58), '!');
characterArraywithoutSpace(59) = strcat(characterArraywithoutSpace(59), '}');
characterArraywithoutSpace(60) = strcat(characterArraywithoutSpace(60), '{');
characterArraywithoutSpace(61) = strcat(characterArraywithoutSpace(61), '(');
characterArraywithoutSpace(62) = strcat(characterArraywithoutSpace(62), ')');
characterArraywithoutSpace(63) = strcat(characterArraywithoutSpace(63), '1');
characterArraywithoutSpace(64) = strcat(characterArraywithoutSpace(64), '2');
characterArraywithoutSpace(65) = strcat(characterArraywithoutSpace(65), '3');
characterArraywithoutSpace(66) = strcat(characterArraywithoutSpace(66), '4');
characterArraywithoutSpace(67) = strcat(characterArraywithoutSpace(67), '5');
characterArraywithoutSpace(68) = strcat(characterArraywithoutSpace(68), '6');
characterArraywithoutSpace(69) = strcat(characterArraywithoutSpace(69), '7');
characterArraywithoutSpace(70) = strcat(characterArraywithoutSpace(70), '8');
characterArraywithoutSpace(71) = strcat(characterArraywithoutSpace(71), '9');
characterArraywithoutSpace(72) = strcat(characterArraywithoutSpace(72), '0');
characterArraywithoutSpace(73) = strcat(characterArraywithoutSpace(73), '&');
characterArraywithoutSpace(74) = strcat(characterArraywithoutSpace(74), '#');
characterArraywithoutSpace(75) = strcat(characterArraywithoutSpace(75), ',');
characterArraywithoutSpace(76) = strcat(characterArraywithoutSpace(76), '.');
characterArraywithoutSpace(77) = strcat(characterArraywithoutSpace(77), '"');
characterArraywithoutSpace(78) = strcat(characterArraywithoutSpace(78), ';');
characterArraywithoutSpace(79) = strcat(characterArraywithoutSpace(79), ':');

%the variable for with space will have to be assigned manual as it is unable to work with 'space' element in the string 
characterArraywithSpace(1) = strcat(characterArraywithSpace(1), ' A');
characterArraywithSpace(2) = strcat(characterArraywithSpace(2), ' B');
characterArraywithSpace(3) = strcat(characterArraywithSpace(3), ' C');
characterArraywithSpace(4) = strcat(characterArraywithSpace(4), ' D');
characterArraywithSpace(5) = strcat(characterArraywithSpace(5), ' E');
characterArraywithSpace(6) = strcat(characterArraywithSpace(6), ' F');
characterArraywithSpace(7) = strcat(characterArraywithSpace(7), ' G');
characterArraywithSpace(8) = strcat(characterArraywithSpace(8), ' H');
characterArraywithSpace(9) = strcat(characterArraywithSpace(9), ' I');
characterArraywithSpace(10) = strcat(characterArraywithSpace(10), ' J');
characterArraywithSpace(11) = strcat(characterArraywithSpace(11), ' K');
characterArraywithSpace(12) = strcat(characterArraywithSpace(12), ' L');
characterArraywithSpace(13) = strcat(characterArraywithSpace(13), ' M');
characterArraywithSpace(14) = strcat(characterArraywithSpace(14), ' N');
characterArraywithSpace(15) = strcat(characterArraywithSpace(15), ' O');
characterArraywithSpace(16) = strcat(characterArraywithSpace(16), ' P');
characterArraywithSpace(17) = strcat(characterArraywithSpace(17), ' Q');
characterArraywithSpace(18) = strcat(characterArraywithSpace(18), ' R');
characterArraywithSpace(19) = strcat(characterArraywithSpace(19), ' S');
characterArraywithSpace(20) = strcat(characterArraywithSpace(20), ' T');
characterArraywithSpace(21) = strcat(characterArraywithSpace(21), ' U');
characterArraywithSpace(22) = strcat(characterArraywithSpace(22), ' V');
characterArraywithSpace(23) = strcat(characterArraywithSpace(23), ' W');
characterArraywithSpace(24) = strcat(characterArraywithSpace(24), ' X');
characterArraywithSpace(25) = strcat(characterArraywithSpace(25), ' Y');
characterArraywithSpace(26) = strcat(characterArraywithSpace(26), ' Z');
characterArraywithSpace(27) = strcat(characterArraywithSpace(27), ' a');
characterArraywithSpace(28) = strcat(characterArraywithSpace(28), ' b');
characterArraywithSpace(29) = strcat(characterArraywithSpace(29), ' c');
characterArraywithSpace(30) = strcat(characterArraywithSpace(30), ' d');
characterArraywithSpace(31) = strcat(characterArraywithSpace(31), ' e');
characterArraywithSpace(32) = strcat(characterArraywithSpace(32), ' f');
characterArraywithSpace(33) = strcat(characterArraywithSpace(33), ' g');
characterArraywithSpace(34) = strcat(characterArraywithSpace(34), ' h');
characterArraywithSpace(35) = strcat(characterArraywithSpace(35), ' i');
characterArraywithSpace(36) = strcat(characterArraywithSpace(36), ' j');
characterArraywithSpace(37) = strcat(characterArraywithSpace(37), ' k');
characterArraywithSpace(38) = strcat(characterArraywithSpace(38), ' l');
characterArraywithSpace(39) = strcat(characterArraywithSpace(39), ' m');
characterArraywithSpace(40) = strcat(characterArraywithSpace(40), ' n');
characterArraywithSpace(41) = strcat(characterArraywithSpace(41), ' o');
characterArraywithSpace(42) = strcat(characterArraywithSpace(42), ' p');
characterArraywithSpace(43) = strcat(characterArraywithSpace(43), ' q');
characterArraywithSpace(44) = strcat(characterArraywithSpace(44), ' r');
characterArraywithSpace(45) = strcat(characterArraywithSpace(45), ' s');
characterArraywithSpace(46) = strcat(characterArraywithSpace(46), ' t');
characterArraywithSpace(47) = strcat(characterArraywithSpace(47), ' u');
characterArraywithSpace(48) = strcat(characterArraywithSpace(48), ' v');
characterArraywithSpace(49) = strcat(characterArraywithSpace(49), ' w');
characterArraywithSpace(50) = strcat(characterArraywithSpace(50), ' x');
characterArraywithSpace(51) = strcat(characterArraywithSpace(51), ' y');
characterArraywithSpace(52) = strcat(characterArraywithSpace(52), ' z');
characterArraywithSpace(53) = strcat(characterArraywithSpace(53), ' /');
characterArraywithSpace(54) = strcat(characterArraywithSpace(54), ' -');
characterArraywithSpace(55) = strcat(characterArraywithSpace(55), ' +');
characterArraywithSpace(56) = strcat(characterArraywithSpace(56), ' *');
characterArraywithSpace(57) = strcat(characterArraywithSpace(57), ' ?');
characterArraywithSpace(58) = strcat(characterArraywithSpace(58), ' !');
characterArraywithSpace(59) = strcat(characterArraywithSpace(59), ' }');
characterArraywithSpace(60) = strcat(characterArraywithSpace(60), ' {');
characterArraywithSpace(61) = strcat(characterArraywithSpace(61), ' (');
characterArraywithSpace(62) = strcat(characterArraywithSpace(62), ' )');
characterArraywithSpace(63) = strcat(characterArraywithSpace(63), ' 1');
characterArraywithSpace(64) = strcat(characterArraywithSpace(64), ' 2');
characterArraywithSpace(65) = strcat(characterArraywithSpace(65), ' 3');
characterArraywithSpace(66) = strcat(characterArraywithSpace(66), ' 4');
characterArraywithSpace(67) = strcat(characterArraywithSpace(67), ' 5');
characterArraywithSpace(68) = strcat(characterArraywithSpace(68), ' 6');
characterArraywithSpace(69) = strcat(characterArraywithSpace(69), ' 7');
characterArraywithSpace(70) = strcat(characterArraywithSpace(70), ' 8');
characterArraywithSpace(71) = strcat(characterArraywithSpace(71), ' 9');
characterArraywithSpace(72) = strcat(characterArraywithSpace(72), ' 0');
characterArraywithSpace(73) = strcat(characterArraywithSpace(73), ' &');
characterArraywithSpace(74) = strcat(characterArraywithSpace(74), ' #');
characterArraywithSpace(75) = strcat(characterArraywithSpace(75), ' ,');
characterArraywithSpace(76) = strcat(characterArraywithSpace(76), ' .');
characterArraywithSpace(77) = strcat(characterArraywithSpace(77), ' "');
characterArraywithSpace(78) = strcat(characterArraywithSpace(78), ' ;');
characterArraywithSpace(79) = strcat(characterArraywithSpace(78), ' :');

for count = 1:loopSize
    
    %to store the value of the line in which the image is representated in
    whichLine = count;
    
    %the boundingbox values of the current image are taken
    tempCenter = newcharacterArray(count).BoundingBox;
    
    %if the loop counter is greater than the number of lines
    if(count >numberOfLines)
        %we check which line the new image might be taken from
        for whichLine = 1:numberOfLines
            if (tempCenter(2) >= startHere(whichLine) && tempCenter(2)<=endHere(whichLine))
                break;
            end
        end
    end
    
    %variable updated with the new current variables value in the
    %respectivbe line
    centerCurrent(whichLine,:) = newcharacterArray(count).BoundingBox;
    
    %flag to check if 'space' should be added between the images or not
    spaceToAddOrNot = 0;
    
    %checks if the distance between previous value and the new value falls
    %below a threshold or not, if value > threshold, there should be a
    %space between the characters. The topright corner value of previous image and the
    %topleft corner values of the new image are compared
    if((abs(centerCurrent(whichLine,1) - (centerPreviois(whichLine,1) + centerPreviois(whichLine,3))) > 4) && count>numberOfLines)
        spaceToAddOrNot = 1;
    else
        spaceToAddOrNot = 0;
    end
    
    %to check the total elapsed time
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    %to count the minutes
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    %if <59 seconds used, the output will be shown accordingly
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    %waitbar output updated
    waitbar(count/loopSize, barToWait, totalTime)
    
    %resize the image to deal with font size issue
    characterToCompare = imresize(newcharacterArray(count).Image,[100 100]);
    
    %checks the image difference of extracted image with all the images in
    %the database
    dist(1) = DistanceFrom(characterToCompare, A);
    dist(2) = DistanceFrom(characterToCompare, B);
    dist(3) = DistanceFrom(characterToCompare, C);
    dist(4) = DistanceFrom(characterToCompare, D);
    dist(5) = DistanceFrom(characterToCompare, E);
    dist(6) = DistanceFrom(characterToCompare, F);
    dist(7) = DistanceFrom(characterToCompare, G);
    dist(8) = DistanceFrom(characterToCompare, H);
    dist(9) = DistanceFrom(characterToCompare, I);
    dist(10) = DistanceFrom(characterToCompare, J);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(11) = DistanceFrom(characterToCompare, K);
    dist(12) = DistanceFrom(characterToCompare, L);
    dist(13) = DistanceFrom(characterToCompare, M);
    dist(14) = DistanceFrom(characterToCompare, N);
    dist(15) = DistanceFrom(characterToCompare, O);
    dist(16) = DistanceFrom(characterToCompare, P);
    dist(17) = DistanceFrom(characterToCompare, Q);
    dist(18) = DistanceFrom(characterToCompare, R);
    dist(19) = DistanceFrom(characterToCompare, S);
    dist(20) = DistanceFrom(characterToCompare, T);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(21) = DistanceFrom(characterToCompare, U);
    dist(22) = DistanceFrom(characterToCompare, V);
    dist(23) = DistanceFrom(characterToCompare, W);
    dist(24) = DistanceFrom(characterToCompare, X);
    dist(25) = DistanceFrom(characterToCompare, Y);
    dist(26) = DistanceFrom(characterToCompare, Z);
    dist(27) = DistanceFrom(characterToCompare, a);
    dist(28) = DistanceFrom(characterToCompare, b);
    dist(29) = DistanceFrom(characterToCompare, c);
    dist(30) = DistanceFrom(characterToCompare, d);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(31) = DistanceFrom(characterToCompare, e);
    dist(32) = DistanceFrom(characterToCompare, f);
    dist(33) = DistanceFrom(characterToCompare, g);
    dist(34) = DistanceFrom(characterToCompare, h);
    dist(35) = DistanceFrom(characterToCompare, i);
    dist(36) = DistanceFrom(characterToCompare, j);
    dist(37) = DistanceFrom(characterToCompare, k);
    dist(38) = DistanceFrom(characterToCompare, l);
    dist(39) = DistanceFrom(characterToCompare, m);
    dist(40) = DistanceFrom(characterToCompare, n);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(41) = DistanceFrom(characterToCompare, o);
    dist(42) = DistanceFrom(characterToCompare, p);
    dist(43) = DistanceFrom(characterToCompare, q);
    dist(44) = DistanceFrom(characterToCompare, r);
    dist(45) = DistanceFrom(characterToCompare, s);
    dist(46) = DistanceFrom(characterToCompare, t);
    dist(47) = DistanceFrom(characterToCompare, u);
    dist(48) = DistanceFrom(characterToCompare, v);
    dist(49) = DistanceFrom(characterToCompare, w);
    dist(50) = DistanceFrom(characterToCompare, x);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(51) = DistanceFrom(characterToCompare, y);
    dist(52) = DistanceFrom(characterToCompare, z);
    dist(53) = DistanceFrom(characterToCompare, scforwardslash);
    dist(54) = DistanceFrom(characterToCompare, schyfen);
    dist(55) = DistanceFrom(characterToCompare, scplus);
    dist(56) = DistanceFrom(characterToCompare, scasterik);
    dist(57) = DistanceFrom(characterToCompare, scquestion);
    dist(58) = DistanceFrom(characterToCompare, scexclamation);
    dist(59) = DistanceFrom(characterToCompare, sccurlyclose);
    dist(60) = DistanceFrom(characterToCompare, sccurlyopen);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(61) = DistanceFrom(characterToCompare, scroundopen);
    dist(62) = DistanceFrom(characterToCompare, scroundclose);
    dist(63) = DistanceFrom(characterToCompare, scOne);
    dist(64) = DistanceFrom(characterToCompare, scTwo);
    dist(65) = DistanceFrom(characterToCompare, scThree);
    dist(66) = DistanceFrom(characterToCompare, scFour);
    dist(67) = DistanceFrom(characterToCompare, scFive);
    dist(68) = DistanceFrom(characterToCompare, scSix);
    dist(69) = DistanceFrom(characterToCompare, scSeven);
    dist(70) = DistanceFrom(characterToCompare, scEight);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
    
    dist(71) = DistanceFrom(characterToCompare, scNine);
    dist(72) = DistanceFrom(characterToCompare, scZero);
    dist(73) = DistanceFrom(characterToCompare, scamp);
    dist(74) = DistanceFrom(characterToCompare, schash);
    dist(75) = DistanceFrom(characterToCompare, sccomma);
    dist(76) = DistanceFrom(characterToCompare, scperiod);
    dist(77) = DistanceFrom(characterToCompare, scquotation);
    dist(78) = DistanceFrom(characterToCompare, scsemicolon);
    dist(79) = DistanceFrom(characterToCompare, sccolon);
    
    %after comparison, the variable with the minimum differenc is selected
    [~, minDist] = min(dist);
    
    %after the minimum difference index is achieved, the value is taken from the respective variable for with space and without space
    if (spaceToAddOrNot == 1)
        string(whichLine) = strcat(string(whichLine),characterArraywithSpace(minDist));
    else
        string(whichLine) = strcat(string(whichLine),characterArraywithoutSpace(minDist));
    end
    
    %the variable for previous value updated accordingly
    centerPreviois(whichLine,:) = centerCurrent(whichLine,:);
    
    endTime = datestr(now, 'HH:MM:SS');
    endTime = [num2str(YearMonthDay(1:3)),' ',endTime];
    endTime =  datevec(endTime,'yyyy mm dd HH:MM:SS');
    
    totalTime = etime(endTime,startTime);
    
    minutes = 0;
    while totalTime>59
        minutes = minutes +1;
        totalTime = totalTime-60;
    end
    
    if(minutes>0)
        totalTime = ['Time elapsed: ',num2str(minutes),' minutes and ', num2str(totalTime) , ' seconds'];
    else
        totalTime = ['Time elapsed: ', num2str(totalTime) , ' seconds'];
    end
    
    waitbar(count/loopSize, barToWait, totalTime)
end

%the waitbar is closed, as the processing has been completed
close(barToWait)

%writing the output into a file
ID = fopen('ReadImageFile.txt', 'wt');
[nrows] = size(string);
for row = 1:nrows
    fprintf(ID,'%s \n',string{row,:});
end
fclose(ID);

end