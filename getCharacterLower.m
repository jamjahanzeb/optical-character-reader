function [ newCharacterArray ] = getCharacterLower(characterArray, threshold)

%get the centroid, image and bounding box of the images in characterArray
newCharacterArray = regionprops(characterArray, {'Image', 'Centroid', 'BoundingBox'});

for k = 1:3
    
    %the below is the check to see if the two consecutive images belong
    %to same character or not
    deleteSomecharacters = 0;
    
    %loop to the final image
    for i = 1:(length(newCharacterArray)-1)
        character1 = newCharacterArray(i).Centroid;
        charatcer2 = newCharacterArray(i+1).Centroid;
        
        %check if the two images have centroid very close to each other or
        %not, check against a threshold
        if(abs(character1(1) - charatcer2(1)) < threshold)
            val1 = newCharacterArray(i).BoundingBox;
            val2 = newCharacterArray(i+1).BoundingBox;
            
            %get the minimum of the lefttop corner
            [valX] = min(val1(1), val2(1));
            [valY] = min(val1(2), val2(2));
            valX = floor(valX)+1;
            valY = floor(valY)+1;
            
            %get the maximum rightbottom corner
            [valXW] = max(val1(1)+val1(3), val2(1)+val2(3));
            [valYW] = max(val1(2)+val1(4), val2(2)+val2(4));
            
            %the previous image is replaced with the new image
            newCharacterArray(i).Image = characterArray(valY:valYW,valX:valXW,:);
            
            %the index of the repeated image is stored to be deleted later
            whatToDelete(deleteSomecharacters) = i+1;
            
            %the flag is raised to make sure that there are images to
            %delete 
            deleteSomecharacters = 1;
        end
    end
    
    if(deleteSomecharacters == 1)
        
        %delete the images in reverse order, so that the incorrect image index is
        %not deleted by accident
        for i = 1:length(whatToDelete)
            newCharacterArray(whatToDelete(length(whatToDelete)-i+1)) = [];
        end
        
        %delete the array containing the indexes, to be reused later
        whatToDelete = [];
    end
end

end