function [distance] = DistanceFrom(character, compareArray)

%loop to compare the character with all images in compareArray
for i=1:length(compareArray)
    %find the differece in the images
    D = imabsdiff(character, compareArray(i).Image);
    
    %stores the difference in a variable
    dist(i) = sum(D(:));
end

%the minimum distance from all the images is returned
[distance] = min(dist);
end