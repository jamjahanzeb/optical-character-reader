function [startHere, endHere] = readlines(I2)

%get the boundingBox of the images
val1 = I2(1).BoundingBox;

%store the value of lefttop corner
startHere(1) = val1(2);

%store the value of the rightbottom corner
endHere(1) = val1(2)+val1(4);

%check to set if the characters are repeating from the same line
breakit = 0;

for i = 2:length(I2)
    for j= 1:length(startHere)
        
        %check if the images are falling the same lines or not
        if((I2(i).Centroid(2) >= startHere(j)) && (I2(i).Centroid(2) <= endHere(j)) || (i==length(I2)))
            breakit = 1;
            break;
        end
    end
    
    %if the image is not in new line
    if(breakit == 1)
        break;
    else
        %else add the lefttop corner and rightbottom corner in the array
        val1 = I2(i).BoundingBox;
        
        startHere(i) = val1(2);
        endHere(i) = val1(2)+val1(4);
    end
end
end