In the course of Image Processing for Recognition, having course code 4577, which was offered for the semester of FALL 2015, the students were given a term project. The purpose of the project was to be able to make an Optical Character Recognizer (OCR), and the following tasks were to be fulfilled:


1. The program should be able to recognize office fonts
2. The program should be able to deal with multiple lines in the given image
3. The program should be able to deal with uppercase letter, lowercase letters and special characters.
4. Produce a documentation to help the user work through the project

This project contains:

1. Testing images
2. Database
3. Documentation - also highlights how to use the project
4. MATLAB code itself

This is an open repository for anyone who wishes to use and make further progress on this project.